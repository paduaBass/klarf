import React, { useState, useEffect } from 'react';
import { Alert, View, StyleSheet, Text, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView, Platform, Image } from 'react-native';
import api from '../services/api';


// import { Container } from './styles';

export default function pages({ navigation }) {
    

    const [hora, setHora] = useState();
    const [data, setData] = useState();
    const [erro, setErro] = useState('');
 


    async function handleSubmit() {
        if(data != null && hora != null ){

   
        try{
            const response = await api.post('/agendamento', {
                data,
                hora
            });
            console.log(response.data);
  //          setNome(null);setTelefone(null);setEmail(null);setEndereco(null);setCidade(null);setEstado(null);setUf(null);
            navigation.navigate('ListUser');

        }
        
        catch(e){
            setErro('feil ved tilkobling til server!');
            setTimeout(() => { setErro(''); }, 3000);
        }
    }
    else {
        setErro('fyll ut alle obligatoriske felt!');
        setTimeout(() => {setErro('');}, 3000);
    }
    }

  return (
    
    <KeyboardAvoidingView enabled={Platform.OS == 'ios', 'android'} behavior='padding' style={styles.container}>

        <View style={styles.form}>
        <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.cabecalho}>
                   
                </View>
                <Text style={styles.titulo}>Planlegg avtale</Text>

                <Text style={styles.erro}>{erro}</Text>
           
            <Text style={styles.label}>Dato *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Dato"
                placeholderTextColor="#999"
                keyboardType="number-pad"
                value={data}
                onChangeText={setData}
            />

            <Text style={styles.label}>Tid *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Tid"
                placeholderTextColor="#999"
                keyboardType="number-pad"
                value={hora}
                onChangeText={setHora}
            />

            <TouchableOpacity onPress={handleSubmit} style={styles.button} >
                <Text style={styles.buttonText}>PLAN</Text>
            </TouchableOpacity>
            </ScrollView>
        </View>
      
    </KeyboardAvoidingView>  
    

  );
}

const styles = StyleSheet.create({
    
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    img: {
        marginTop: 20,
        alignItems: "center",
        marginBottom: 20,


    },
    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        marginTop: 10,  
    },

    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },

    input: {
        borderWidth: 1,
        borderColor: '#B0E0E6',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },

    button: {
        height: 42,
        backgroundColor: '#00BFFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 20
    },

    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,

    },
    titulo: {
        fontSize: 25,
        fontWeight: "bold",
        color: "#FF7F50",
        marginBottom: 10,
        textAlign: "center",
        marginBottom: 10
    },
    cabecalho: {
        justifyContent: "center",
        marginTop: 20,
        flexDirection: "row"
    },
    espaco: {
        marginHorizontal:15,
    },
    erro: {
        color: 'red',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 18,
    }
});