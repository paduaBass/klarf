import React, {useEffect, useState} from 'react';
import { View, Alert, StyleSheet, Text, FlatList, Image, TouchableOpacity  } from 'react-native';
import api from '../services/api';
import Lista from '../components/Lista';


// import { Container } from './styles';

export default function ListUser({navigation}) {
    const [response = "Carregando dados!", setResponse] = useState([]);
    
    async function novoAgendamento(){
        navigation.navigate('Agendamento');
    }
    
    useEffect(() => {
        async function loadData(){
            try{
            
                const response = await api.get("/agendamentos");
                setResponse(response);
                
                console.log(response.data);

            }
            catch(e){
               console.log(e);
            }
        }
        loadData();
    },[])
    
  return (
   <View style={styles.container}>
       <TouchableOpacity style={styles.button} onPress={novoAgendamento}><Text style={styles.buttonText}>Planlegg avtale</Text></TouchableOpacity>
     
    
        <Lista dataList={response.data} item={item => String(item._id)}></Lista>
        
      
   </View>
  );
}


const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#fff"
    },
    img: {
        marginTop: 20,
        alignItems: "center",
        marginBottom: 20,


    },
    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        marginTop: 30,  
    },

    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },

    input: {
        borderWidth: 1,
        borderColor: '#ddd',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },

    button: {
        marginTop: 5,
        width: 400,
        height: 42,
        backgroundColor: '#00BFFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 20
    },

    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,

    },
    title: {
        fontWeight: "bold",
        fontSize: 25,
        color: "#FF7F50"
    },
    titulo: {
        fontSize: 25,
        fontWeight: "bold",
        color: "#FF7F50",
        marginBottom: 10,
        textAlign: "center",
        marginBottom: 10
    },
    cabecalho: {
        justifyContent: "center",
        marginTop: 29,
        flexDirection: "row"
    },
    espaco: {
        marginHorizontal:15,
    }
});
