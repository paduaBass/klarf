import React, { useState, useEffect } from 'react';
import { Alert, View, StyleSheet, Text, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView, Platform, Image } from 'react-native';
import api from '../services/api';


// import { Container } from './styles';

export default function pages({ navigation }) {
    
    const [nome, setNome] = useState();
    const [sobrenome, setSobrenome] = useState();
    const [email, setEmail] = useState();
    const [dataDeNascimento, setDataDeNascimento] = useState();
    const [telefone, setTelefone] = useState();
    const [celular, setCelular] = useState();
    const [cidade, setCidade] = useState();
    const [endereco, setEndereco] = useState();
    const [caixaPostal, setCaixaPostal] = useState();
    const [ nomeResponsavel, setNomeResponsavel] = useState();
    const [sobrenomeResponsavel, setSobrenomeResponsavel] = useState();
    const [telefoneResponsavel, setTelefoneResponsavel] = useState();
    const [emailResponsavel, setEmailResponsavel] = useState();
    const [senha, setSenha] = useState();


    async function handleSubmit() {
        if(nome != null && sobrenome != null && celular != null && email != null && endereco != null && dataDeNascimento != null ){

   
        try{
            const response = await api.post('/new', {
                nome,
                sobrenome,
                email,
                dataDeNascimento,
                telefone,
                celular,
                cidade,
                endereco,
                caixaPostal,
                nomeResponsavel,
                sobrenomeResponsavel,
                telefoneResponsavel,
                emailResponsavel,
                senha,
            });
            console.log(response.data);
  //          setNome(null);setTelefone(null);setEmail(null);setEndereco(null);setCidade(null);setEstado(null);setUf(null);
            navigation.navigate('Entrar');

        }
        catch(e){
            Alert.alert("feil ved tilkobling til server!");
        }
    }
    else {
        Alert.alert("fyll ut alle obligatoriske felt!");
    }
    }

  return (
    
    <KeyboardAvoidingView enabled={Platform.OS == 'ios', 'android'} behavior='padding' style={styles.container}>

        <View style={styles.form}>
        <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.cabecalho}>
                   
                </View>
                <Text style={styles.titulo}>Matrikkelen</Text>

            <Text style={styles.label}>NAVN *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Navn"
                placeholderTextColor="#999"
                autoCapitalize="words"
                autoCorrect={false}
                value={nome}
                onChangeText={setNome}
            />

            <Text style={styles.label}>ETTERNAVN *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Etternavn"
                placeholderTextColor="#999"
                value={sobrenome}
                onChangeText={setSobrenome}
            />

            <Text style={styles.label}>E-POST *</Text>
            <TextInput 
                style={styles.input}
                placeholder="E-post"
                placeholderTextColor="#999"
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                value={email}
                onChangeText={setEmail}
            />

            <Text style={styles.label}>Fødselsnummer *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Fødselsnummer"
                placeholderTextColor="#999"
                keyboardType="number-pad"
                value={dataDeNascimento}
                onChangeText={setDataDeNascimento}
            />

            <Text style={styles.label}>TELEFON</Text>
            <TextInput 
                style={styles.input}
                placeholder="Telefon"
                placeholderTextColor="#999"
                keyboardType="number-pad"
                value={telefone}
                onChangeText={setTelefone}
            />

            <Text style={styles.label}>MOBIL *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Mobil"
                placeholderTextColor="#999"
                keyboardType="number-pad"
                value={celular}
                onChangeText={setCelular}
            />

            <Text style={styles.label}>ADRESS *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Adress"
                placeholderTextColor="#999"
                autoCapitalize="words"
                autoCorrect={false}
                value={endereco}
                onChangeText={setEndereco}
            />
            
            <Text style={styles.label}>PASSORD *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Passord"
                placeholderTextColor="#999"
                secureTextEntry={true}
                password={true}
                autoCorrect={false}
                value={senha}
                onChangeText={setSenha}
            />

            <TouchableOpacity onPress={handleSubmit} style={styles.button} >
                <Text style={styles.buttonText}>REGISTRER</Text>
            </TouchableOpacity>
            </ScrollView>
        </View>
      
    </KeyboardAvoidingView>  
    

  );
}

const styles = StyleSheet.create({
    
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    img: {
        marginTop: 20,
        alignItems: "center",
        marginBottom: 20,


    },
    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        marginTop: 10,  
    },

    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },

    input: {
        borderWidth: 1,
        borderColor: '#B0E0E6',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },

    button: {
        height: 42,
        backgroundColor: '#00BFFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 20
    },

    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,

    },
    titulo: {
        fontSize: 25,
        fontWeight: "bold",
        color: "#FF7F50",
        marginBottom: 10,
        textAlign: "center",
        marginBottom: 10
    },
    cabecalho: {
        justifyContent: "center",
        marginTop: 20,
        flexDirection: "row"
    },
    espaco: {
        marginHorizontal:15,
    }
});