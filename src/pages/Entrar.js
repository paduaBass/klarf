import React, { useState, useEffect } from 'react';
import { Alert, View, StyleSheet, Text, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView, Platform, Image } from 'react-native';
import api from '../services/api';


// import { Container } from './styles';

export default function pages({ navigation }) {

    const [email, setEmail] = useState();
    const [senha, setSenha] = useState();
    const [erro, setErro] = useState('');

    function novoCadastro(){
        navigation.navigate('Login');
    }


    async function handleSubmit() {
        if(email != null && senha != null){

   
        try{
            const response = await api.post('/login', {
                email,
                senha,
            });
            console.log(response.data);
  //          setNome(null);setTelefone(null);setEmail(null);setEndereco(null);setCidade(null);setEstado(null);setUf(null);
            
            navigation.navigate('ListUser');

        }
        catch(e){
           setErro('sjekk legitimasjonsbeskrivelsen!');
            setTimeout(()=>{setErro('');}, 3000)
        }
    }
    else {
        setErro('fyll ut alle obligatoriske felt!');
        setTimeout(()=>{setErro('');}, 3000)

       
    }
    }

  return (
    
    <KeyboardAvoidingView enabled={Platform.OS == 'ios', 'android'} behavior='padding' style={styles.container}>

        <View style={styles.form}>
        <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.cabecalho}>
                   
                </View>
                <Text style={styles.titulo}>å gå inn</Text>
                <Text style={styles.erro}>{erro}</Text>

            <Text style={styles.label}>E-POST *</Text>
            <TextInput 
                style={styles.input}
                placeholder="E-post"
                placeholderTextColor="#999"
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                value={email}
                onChangeText={setEmail}
            />

            <Text style={styles.label}>PASSORD *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Passord"
                placeholderTextColor="#999"
                autoCorrect={false}
                password={true}
                autoCapitalize="none"
                secureTextEntry={true}
                value={senha}
                onChangeText={setSenha}
            />


            <TouchableOpacity onPress={handleSubmit} style={styles.button} >
                <Text style={styles.buttonText}>LOGG INN</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={novoCadastro}>
                <Text style={styles.buttonTextCadastrar}>Nytt her? Registrer deg!</Text>
            </TouchableOpacity>
            </ScrollView>
        </View>
      
    </KeyboardAvoidingView>  
    

  );
}

const styles = StyleSheet.create({
    
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    img: {
        marginTop: 20,
        alignItems: "center",
        marginBottom: 20,


    },
    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        marginTop: 10,  
    },

    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },

    input: {
        borderWidth: 1,
        borderColor: '#B0E0E6',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },

    button: {
        height: 42,
        backgroundColor: '#00BFFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 20
    },

    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,

    },
    titulo: {
        fontSize: 25,
        fontWeight: "bold",
        color: "#FF7F50",
        marginBottom: 10,
        textAlign: "center",
        marginBottom: 10
    },
    cabecalho: {
        justifyContent: "center",
        marginTop: 20,
        flexDirection: "row"
    },
    espaco: {
        marginHorizontal:15,
    },
    erro: {
        color: 'red',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 18,
    },
    buttonTextCadastrar: {
        color: '#00BFFF',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        textDecorationLine: 'underline'
    }
});