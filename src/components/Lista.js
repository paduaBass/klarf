import React, { Component } from 'react';

import { View, FlatList, Text, StyleSheet, Dimensions } from 'react-native';

var {width} = Dimensions.get('window');


// import { Container } from './styles';

export default class Lista extends Component {
  render() {
    return <FlatList 
    style={styles.flatList}
    showsVerticalScrollIndicator={false}
    data={this.props.dataList}
    keyExtractor={this.props.item}
    renderItem={ ({ item }) => (
        <View style={styles.container}>
            <View style={styles.list}>
            
            <Text style={styles.title}>Planlegging</Text>
          
            <Text style={styles.options}>
            Dato: {item.data}
            </Text >
           
            <Text style={styles.options}>
            Tid: {item.hora}
            </Text>
           
        
            </View>
        </View>
    )}
    />
  }
}

const styles = StyleSheet.create({
    container: {
        marginTop:60,
        flex:1,
        justifyContent: 'center',
        backgroundColor: "#FFFFFF",
        borderRadius: 5,
        width: width -40,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        
    },
    img: {
        marginTop: 20,
        alignItems: "center",
        marginBottom: 20,


    },
    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        marginTop: 30,  
    },

    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },

    input: {
        borderWidth: 1,
        borderColor: '#ddd',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },

    button: {
        height: 42,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 20
    },

    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,

    },
    title: {
        fontWeight: "bold",
        fontSize: 20,
        textAlign: "center",
        color: "#FF8C00",
        padding: 2,
    },
    list:{        
        padding: 6,
        borderRadius: 5,
        marginBottom: 4,
        
    },
    titleList:{
        borderWidth:1,
        borderRadius: 5,
        borderColor: '#B0E0E6',
    },
    flatList: {
        
    },
    options: {
        fontWeight: "bold",
        padding: 2,

    }
});

