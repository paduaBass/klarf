import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './pages/Login';
import ListUser from './pages/ListUser';
import Agendamento from './pages/Agendamento';
import Entrar from './pages/Entrar';


const Routes = createAppContainer(
    
    createStackNavigator({ 
        Entrar: {
            screen: Entrar,
            navigationOptions: {
                title: "Klar, Ferdig, gå",
                headerTitleStyle:{
                    color: "#fff",
                    
                },
                headerBackTitleStyle:{
                    color: "#fff"
                },
                headerStyle: {
                    backgroundColor: "#FF7F50"
                }
            }
        },
        Login: {
            screen: Login,
            navigationOptions: {
                title: "Klar, Ferdig, gå",
                headerTitleStyle:{
                    color: "#fff",
                    
                },
                headerBackTitleStyle:{
                    color: "#fff"
                },
                headerStyle: {
                    backgroundColor: "#00BFFF"
                }
            }
        },  
        
        ListUser: {
            screen: ListUser,
            navigationOptions: {
                title: "Tidsplaner",
                headerTitleStyle:{
                    color: "#fff",
                    
                },
                headerBackTitleStyle:{
                    color: "#fff"
                },
                headerStyle: {
                    backgroundColor: "#FF7F50"
                }
            }
        },
         Agendamento: {
            screen: Agendamento,
            navigationOptions: {
                title: "Klar, Ferdig, gå",
                headerTitleStyle:{
                    color: "#fff",
                    
                },
                headerBackTitleStyle:{
                    color: "#fff"
                },
                headerStyle: {
                    backgroundColor: "#00BFFF"
                }
            }
        },  
        })
);



export default Routes;